# Code contributions

## Изменения кода
1. Code style

В этом проекте используется несколько инструментов для поддержания единообразного стиля кода:
- black
- isort
- mypy
- ruff

Способ убедиться, что ваш запрос на включение соответствует стилю кода, - установить предварительную фиксацию.
```bash
pip install pre-commit
pre-commit install
```

2. Для проверки соответствия кода установленному стилю до коммита, можно вручную запустить команды.
```bash
pip install ruff
ruff format .
ruff check .
```

3. Стиль именования коммитов

В первую очередь в комментарии к коммиту указываем тип коммита:
- `feat` — используется при добавлении новой функциональности уровня приложения
- `fix` — если исправили какую-то серьезную багу
- `docs` — всё, что касается документации
- `style` — исправляем опечатки, исправляем форматирование
- `refactor` — рефакторинг кода приложения
- `test` — всё, что связано с тестированием
- `chore` — обычное обслуживание кода (лучше не использовать)

Например, коммит на добавление тестирования эксперимента будет выглядеть так:
```bash
git commit -m "test: add testing of yolo model"
```
