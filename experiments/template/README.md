# Гипотеза: Template
- Метрика:
- Данные (версия):
- Интерпретация результатов эксперимента (определение успешности эксперимента):
- Артефакты:

## Запуск эксперимента
- Подготовка окружения
```bash
python -V  # 3.11.7
python -m venv venv
. venv/bin/activate
pip install -U pip==24.0
pip install -r experiments/template/requirements.txt
cp experiments/template/.env.template experiments/template/.env
```
- Заполните `experiments/template/.env`
- Запуск эксперимента
```bash
python run_pipeline.py --config experiments/template/pipeline.yaml
```
